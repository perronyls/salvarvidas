<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Missão Salvar Vidas - Missão, respeito e amor pela Vida</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Serviços de Ambulância 24 horas" />
	<meta name="keywords" content="Ambulância, ambulancia particular, ambulancia particular preco, remoção hospitalar, serviço de ambulancia, serviço de remoção" />
	<meta name="author" content="Scheffer Consultoria Ltda." />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta properaty="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href="https://fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	
	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">


	<link rel="stylesheet" href="css/style.css">


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->


	</head>
	<body>


	

   <div class="container">



		<h1>Estamos em Construção</h1>
	  <h2>Para melhor atendê-lo, em breve estaremos com um novo site!</h2>

      <div style="text-align:center"><img src="/images/logo_missao_salvar_vidas.png" width="30%"></div>
      <p>&nbsp</p>
			<h1>Contato</h1>
			<h3><a href="https://wa.me/5549991647149"><img src="/images/whatsapp-logo.png" width="30rem" /> (49) 99164-7149 (Claudemir)</a></h3>
			<h3><a href="https://wa.me/5549988918305"><img src="/images/whatsapp-logo.png" width="30rem" /> (49) 98891-8305 (Daiane)</a></h3>

		</div>


		
<div id="footer"></div>
		



		
		
	
	
	


	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Counters -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>

	<!-- Main JS (Do not remove) -->
	<script src="js/main.js"></script>
	<script src="js/loader.js"> </script>



	 

	</body>
</html>

